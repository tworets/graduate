import cv2
from environment import imagePath
from convert_image_to_grayscale import convert_to_grayscale
from resize_image import resize_image
from convert_image_to_matrix import convert_image_to_matrix
from dijkstra import find_shortest_path
from red_image import color_path_red

# Convert the input image to grayscale
gray_image = convert_to_grayscale(imagePath)

# Resize the grayscale image to 256x256 pixels
resized_image = resize_image(gray_image, target_size=(256, 256))

matrix = convert_image_to_matrix (resized_image)

print("Matrix Shape: ", matrix.shape)
print("Matrix", matrix)

start_index = (100, 60)
end_index = (160, 100)
path = find_shortest_path(matrix, start_index, end_index)
print(path) 

# Color the path in red
red_image = color_path_red(resized_image, path)

# Display the resulting image
cv2.imshow('Red Path', red_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
