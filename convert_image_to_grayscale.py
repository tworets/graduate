import cv2

def convert_to_grayscale(image_path):
    image = cv2.imread(image_path)
    if image is None:
        raise Exception(f"Image '{image_path}' not found.")
    
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return gray_image