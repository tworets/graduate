import cv2

def color_path_red(image, path):
    # Create a copy of the image to fill with red color
    red_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

    for point in path:
        red_image[point] = [0, 0, 255]

    return red_image