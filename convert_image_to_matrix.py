import numpy as np

def convert_image_to_matrix(gray_image) -> np.ndarray:
    """
    Converts a grayscale image to a matrix where each pixel is represented by its grayscale value.
    More white pixels have a lower weight than more gray pixels.
    """
    matrix = np.zeros(gray_image.shape, dtype=np.float32)
    for i in range(gray_image.shape[0]):
        for j in range(gray_image.shape[1]):
            matrix[i, j] = 1 / (gray_image[i, j] + 1)
    return matrix