import cv2
import numpy as np

def resize_image(image, target_size):
    """
    Resizes the image to the target size while maintaining the aspect ratio.
    If the image is larger than the target size, it will be resized to fit the target size.
    If the image is smaller than the target size, it will be resized to fit the target size.
    """
    # Calculate the aspect ratio of the image
    aspect_ratio = image.shape[1] / image.shape[0]

    # Calculate the target aspect ratio
    target_aspect_ratio = target_size[1] / target_size[0]

    # Check if the image needs to be resized
    if image.shape[0] > target_size[0] or image.shape[1] > target_size[1]:
        # If the image is larger than the target size, resize it to fit the target size
        if aspect_ratio > target_aspect_ratio:
            # If the image has a wider aspect ratio than the target size, resize it to fit the height
            resized_height = target_size[0]
            resized_width = int(image.shape[1] * target_size[0] / image.shape[1])
        else:
            # If the image has a narrower aspect ratio than the target size, resize it to fit the width
            resized_width = target_size[1]
            resized_height = int(image.shape[0] * target_size[1] / image.shape[1])
    else:
        # If the image is smaller than the target size, resize it to fit the target size
        if aspect_ratio > target_aspect_ratio:
            # If the image has a wider aspect ratio than the target size, resize it to fit the width
            resized_width = target_size[1]
            resized_height = int(image.shape[0] * target_size[1] / image.shape[0])
        else:
            # If the image has a narrower aspect ratio than the target size, resize it to fit the height
            resized_width = int(image.shape[1] * target_size[0] / image.shape[0])
            resized_height = target_size[0]

    # Resize the image to the target size
    resized_image = cv2.resize(image, (resized_width, resized_height), interpolation=cv2.INTER_AREA)

    return resized_image